import React from 'react';
import PropTypes from 'prop-types';

const Form = ({ onSubmit, children }) => (
    <form style={{ width: '100%' }} onSubmit={e => {
        e.preventDefault();
        onSubmit();
    }}>
        {children}
    </form>
);

Form.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
};

export default Form;
