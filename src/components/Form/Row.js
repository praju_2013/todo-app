import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Row = ({ children, className }) => (
    <div className={classnames('row', className)}>
        {children}
    </div>
);

Row.propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
}

Row.defaultProps = {
    className: undefined,
}

export default Row;
