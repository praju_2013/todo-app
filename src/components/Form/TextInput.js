import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const TextInput = ({ value, type, onChange, className, ...rest }) => (
    <input
        {...rest} 
        type={type}
        className={classnames('form-control', className)}
        value={value}
        onChange={e => onChange(e.target.value)} />
);

TextInput.propTypes = {
    value: PropTypes.string,
    type: PropTypes.string,
    onChange: PropTypes.func,
    className: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    rest: PropTypes.object,
};

TextInput.defaultProps = {
    value: '',
    type: 'text',
    onChange: undefined,
    className: undefined,
    rest: undefined,
};

export default TextInput
