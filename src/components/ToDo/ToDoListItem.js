import React from 'react';
import PropTypes from 'prop-types';
import Col from '../Form/Col';
import Checkbox from '../Form/Checbox';
import Row from '../Form/Row';
import status from '../../referenceData/status';

const ToDoListItem = ({ toDo, onComplete }) => (
    <Row className="text-left todo-list-item-row">
        <Col size="sm-6">
            {toDo.status === status.COMPLETE ?
                <del>{toDo.value}</del> : toDo.value}
        </Col>
        <Col size="sm-6" className="text-right">
            <Checkbox checked={toDo.status === status.COMPLETE}
                onChange={(checked) => onComplete(checked)} /> Complete
        </Col>
    </Row>
);

ToDoListItem.propTypes = {
    toDo: PropTypes.object.isRequired,
    onComplete: PropTypes.func.isRequired,
}

export default ToDoListItem;
