import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import ToDoInput from '../ToDoInput';
import initialState from '../../../store/initialState';

describe('ToDoInput', () => {
    it('should render correctly and trigger setToDo', () => {
        const setToDoStub = sinon.stub();
        const component = shallow(<ToDoInput toDo={initialState.toDoInput} setToDo={setToDoStub} />);

        expect(component).toMatchSnapshot();
        component.find('TextInput').simulate('change', 'dummy');
        expect(setToDoStub.calledOnce).toBe(true);
    });

    it('should highlight input when value is being entered', () => {
        const setToDoStub = sinon.stub();
        const component = shallow(<ToDoInput toDo={{ ...initialState.toDoInput, value: 'go running' }} setToDo={setToDoStub} />);
        expect(component).toMatchSnapshot();
    });
});