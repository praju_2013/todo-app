import React from 'react';
import { shallow } from 'enzyme';
import Row from '../Row';

describe('Row', () => {
    [
        undefined,
        'p-5',
    ].forEach(className => {
        it('should render correctly', () =>{
            expect(shallow(<Row className={className}>content</Row>)).toMatchSnapshot();
        });
    });
});
