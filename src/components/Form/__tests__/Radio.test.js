import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import Radio from '../Radio';

describe('Radio', () => {
    [
        {},
        { label: 'Active', onClick: sinon.stub(), className: 'm-5' }
    ].forEach(testCase => {
        it(`should render correctly ${testCase.onClick ? 'and trigger onClick' : ''}`, () => {
            const component = shallow(<Radio
                label={testCase.label}
                onClick={testCase.onClick}
                className={testCase.className} />)

            expect(component).toMatchSnapshot();

            if (testCase.onClick) {
                component.find('input').simulate('click', { target: { checked: true } })
                expect(testCase.onClick.calledOnce).toBe(true);
            }
        });
    });
});