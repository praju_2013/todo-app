import React from 'react';
import sinon, { expectation } from 'sinon';
import { shallow } from 'enzyme';
import { ToDo, mapStoreToProps, mapDispatchToProps } from '../';
import status from '../../../referenceData/status';
import initialState from '../../../store/initialState';

describe('ToDo comp', () => {

    const setToDoInput = sinon.stub();
    const setStatusFilter = sinon.stub();
    const setToDoStatus = sinon.stub();
    const addToDo = sinon.stub();

    const state = {
        ...initialState, toDoList: [
            { value: 'buy milk', status: status.ACTIVE },
            { value: 'go running', status: status.COMPLETE },
        ]
    };

    const render = (state) => shallow(<ToDo {...state}
        setStatusFilter={setStatusFilter}
        setToDoInput={setToDoInput}
        setToDoStatus={setToDoStatus}
        addToDo={addToDo} />);

    it('should render correctly', () => {
        const component = render(state);
        expect(component).toMatchSnapshot();

        // it should not call addToDo for empty values
        component.find('Form').simulate('submit');
        expect(addToDo.calledOnce).toBe(false);
    });

    it('should trigger addToDo on submit', () => {
        const component = render({ ...state, toDoInput: { ...initialState.toDoInput, value: 'do nothing' }})
        component.find('Form').simulate('submit');
        expect(addToDo.calledOnce).toBe(true);
    });

    it('should render correctly for filtered ToDos', () => {
        const component = render({ ...state, statusFilter: status.COMPLETE })
        expect(component).toMatchSnapshot();
    });

    it('should mapStoreToProps', () => {

        const mappedProps = mapStoreToProps(initialState);
        expect(mappedProps).toEqual({
            toDoInput: initialState.toDoInput,
            toDoList: initialState.toDoList,
            statusFilter: initialState.statusFilter,
        });
    });

    it('should mapDispatchToProps', () => {
        const dispatchStub = sinon.stub();

        const mappedProps = mapDispatchToProps(dispatchStub);
        mappedProps.setStatusFilter('ACTIVE');
        mappedProps.addToDo('do homework');
        mappedProps.setToDoStatus(0, 'ACTIVE');
        mappedProps.setToDoInput('dummy');
        expect(dispatchStub.callCount).toBe(4);

    });
});