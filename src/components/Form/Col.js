import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

const Col = ({ className, size, children }) => (
    <div className={classnames(`col-${size}`, className)}>
        {children}
    </div>
);

Col.propTypes = {
    className: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    size: PropTypes.string,
    children: PropTypes.node,
};

Col.defaultProps = {
    className: undefined,
    size: 'sm-12',
    children: undefined,
};

export default Col;
