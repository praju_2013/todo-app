import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import Checkbox from '../Checbox';

describe('Checkbox', () => {
    [
        {},
        { label: 'Complete', onChange: sinon.stub(), className: 'm-5' }
    ].forEach(testCase => {
        it(`should render correctly ${testCase.onChange ? 'and trigger onChange' : ''}`, () => {
            const component = shallow(<Checkbox
                label={testCase.label}
                onChange={testCase.onChange}
                className={testCase.className} />)

            expect(component).toMatchSnapshot();

            if (testCase.onChange) {
                component.find('input').simulate('change', { target: { checked: true } })
                expect(testCase.onChange.calledOnce).toBe(true);
            }
        });
    });
});