import React from 'react';
import { Provider } from 'react-redux';
import './App.css';
import ToDo from './components/ToDo';
import store from './store';

function App() {
  return (
    <Provider store={store} >
      <div className="App container justify-content-center">
        <ToDo />
      </div>
    </Provider>
  );
}

export default App;
