import { SET_TODO_INPUT, ADD_TODO, SET_STATUS_FILTER, SET_TODO_STATUS } from '../actions';
import initialState from '../initialState';

const rootReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case SET_TODO_INPUT:
            return { ...state, toDoInput: payload };

        case ADD_TODO: {
            const newToDoList = [...state.toDoList];
            newToDoList.push(payload);
            return { ...state, toDoList: newToDoList, toDoInput: initialState.toDoInput };
        }

        case SET_STATUS_FILTER:
            return { ...state, statusFilter: payload };

        case SET_TODO_STATUS: {
            const newToDoList = [...state.toDoList];
            newToDoList[payload.index].status = payload.status;
            return { ...state, toDoList: newToDoList };
        }

        default:
            return state;
    }
}

export default rootReducer;
