import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import initialState from '../../../store/initialState';
import ToDoListItem from '../ToDoListItem';
import status from '../../../referenceData/status';

describe('ToDoListItem', () => {
    const onCompleteStub = sinon.stub();
    it('should render correctly and trigger setToDo', () => {
        const component = shallow(<ToDoListItem
            toDo={{ ...initialState.toDoInput, value: 'buy milk' }}
            onComplete={onCompleteStub}
        />);

        expect(component).toMatchSnapshot();
        component.find('Checkbox').simulate('change', true);
        expect(onCompleteStub.calledOnce).toBe(true);
    });

    it('should render correctly for complete ToDo', () => {
        const component = shallow(<ToDoListItem
            toDo={{ ...initialState.toDoInput, value: 'buy milk', status: status.COMPLETE }}
            onComplete={onCompleteStub}
        />);

        expect(component).toMatchSnapshot();
    })
});