import { addToDo, setSignUpData, setStatusFilter, setToDoInput, setToDoStatus } from '../../actions';
import reducer from '../';
import initialState from '../../initialState';
import status from '../../../referenceData/status';

describe('root reducer', () => {
    it('should return default state properly', () => {
        const action = { type: 'UNKNOWN' };
        const updatedState = reducer(undefined, action);
        expect(updatedState).toEqual(initialState);
    });

    it('should handle SET_TODO_INPUT', () => {
        const action = setToDoInput('go running');
        const updatedState = reducer(initialState, action);
        expect(updatedState).toEqual({ ...initialState, toDoInput: 'go running' });
    });

    it('should handle ADD_TODO', () => {
        const action = addToDo({ value: 'go running', status: status.ACTIVE });
        const updatedState = reducer(initialState, action);
        expect(updatedState).toEqual({ ...initialState, toDoList: [{ value: 'go running', status: status.ACTIVE }] });
    });

    it('should handle SET_STATUS_FILTER', () => {
        const action = setStatusFilter(status.ACTIVE);
        const updatedState = reducer(initialState, action);
        expect(updatedState).toEqual({ ...initialState, statusFilter: status.ACTIVE });
    });

    it('should handle SET_TODO_STATUS', () => {
        const action = setToDoStatus(0, status.COMPLETE);
        const currentState = { ...initialState, toDoList: [{ value: 'study', status: status.ACTIVE }] };
        const updatedState = reducer(currentState, action);
        expect(updatedState).toEqual({ ...initialState, toDoList: [{ value: 'study', status: status.COMPLETE }] });
    });
});