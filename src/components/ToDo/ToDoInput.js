import React from 'react';
import TextInput from '../Form/TextInput';

const ToDoInput = ({ toDo, setToDo }) => (
    <TextInput style={{
            borderColor: toDo.value === '' ? '#ced4da' : '#80bdff',
            boxShadow: toDo.value === '' ? 'none' : undefined,
        }}
        className="col-sm-12"
        value={toDo.value} onChange={val => setToDo({ ...toDo, value: val })}
        placeholder="add todo..."
    />
);

export default ToDoInput;
