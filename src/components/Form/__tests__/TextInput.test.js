import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import TextInput from '../TextInput';

describe('TextInput', () => {
    [
        {},
        { type: 'number', value: 'buy milk', onChange: sinon.stub(), className: 'm-5', placeholder: "add todo" }
    ].forEach(testCase => {
        it(`should render correctly ${testCase.onChange ? ' and trigger onChange' : ''}`, () => {
            const component = shallow(<TextInput
                type={testCase.type}
                value={testCase.value}
                onChange={testCase.onChange}
                className={testCase.className} />)

            expect(component).toMatchSnapshot();

            if (testCase.onChange) {
                component.find('input').simulate('change', { target: { value: 'dummy' } })
                expect(testCase.onChange.calledOnce).toBe(true);
            }
        });
    });
});
