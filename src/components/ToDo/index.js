import React from 'react';
import { connect } from 'react-redux';
import { setToDoInput, addToDo, setToDoStatus, setStatusFilter } from '../../store/actions';
import Row from '../Form/Row';
import './ToDo.css';
import ToDoFilterMenu from './ToDoFilterMenu';
import ToDoInput from './ToDoInput';
import ToDoListItem from './ToDoListItem';
import status from '../../referenceData/status';
import Form from '../Form/Form';

export const ToDo = ({ toDoInput, setToDoInput, toDoList, addToDo,
    setToDoStatus, statusFilter, setStatusFilter }) => (
        <>
            <h1 className="mt-5">ToDos</h1>
            {/* main add todo Section */}
            <div className="mt-2 ml-5 todo-wrapper">
                <Form onSubmit={() => {
                    if (!toDoInput.value || !toDoInput.value.trim()) {
                        return false;
                    }
                    addToDo(toDoInput);
                }}>

                    {/* input for todo */}
                    <Row>
                        <ToDoInput toDo={toDoInput} setToDo={setToDoInput} />
                    </Row>

                    {/* display todos by statuses */}
                    {toDoList.map((toDo, index) => (statusFilter === status.ALL || statusFilter === toDo.status) ?
                        <ToDoListItem  key={`todo-${index}`} toDo={toDo}
                            onComplete={(isComplete) => setToDoStatus(index, isComplete ? status.COMPLETE : status.ACTIVE)} />
                        : undefined,
                    )}

                    {/* filter todos by statuses */}
                    <ToDoFilterMenu statusFilter={statusFilter} setStatusFilter={setStatusFilter} />
                </Form>
            </div>
        </>
    );

export const mapStoreToProps = ({ toDoInput, toDoList, statusFilter }) => ({
    toDoInput, toDoList, statusFilter
});

export const mapDispatchToProps = dispatch => ({
    setToDoInput: (toDoInput) => dispatch(setToDoInput(toDoInput)),
    addToDo: (toDo, index) => dispatch(addToDo(toDo, index)),
    setToDoStatus: (status, index) => dispatch(setToDoStatus(status, index)),
    setStatusFilter: (statusFilter) => dispatch(setStatusFilter(statusFilter)),
});

export default connect(mapStoreToProps, mapDispatchToProps)(ToDo);