import React from 'react';
import { shallow } from 'enzyme';
import Col from '../Col';

describe('Col', () => {
    [
        { size: 'sm-6', className: 'p-5' },
        {},
    ].forEach(({ size, className }) => {
        it(`should render correctly with size as ${size} and className ${className}`, () => {
            expect(shallow(<Col size={size} className={className}>content</Col>)).toMatchSnapshot();
        });
    });
});
