import React from 'react';
import PropTypes from 'prop-types';

const Radio = ({ checked, name, onClick, label, className, ...rest }) => (
    <>
        <input {...rest} name={name} checked={checked} className={className}
            type="radio" onClick={e => onClick(e)} /> {label}
    </>
);

Radio.propTypes = {
    checked: PropTypes.bool,
    name: PropTypes.string,
    onClick: PropTypes.func,
    label: PropTypes.string,
    className: PropTypes.string,
    rest: PropTypes.object,
}

Radio.defaultProps = {
    checked: undefined,
    name: undefined,
    onClick: undefined,
    label: undefined,
    className: undefined,
    rest: undefined,
}

export default Radio;
