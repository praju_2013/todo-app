import status from '../referenceData/status';

export default {
    toDoInput: {
        value: '',
        status: status.ACTIVE
    },
    toDoList: [],
    statusFilter: status.ALL,
}