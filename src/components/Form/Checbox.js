import React from 'react';
import PropTypes from 'prop-types';

const Checkbox = ({ className, label, checked, onChange }) => (
    <>
        <input type="checkbox" className={className} checked={checked}
            onChange={(e) => onChange(e.target.checked)} /> {label}
    </>
);

Checkbox.propTypes = {
    className: PropTypes.string,
    label: PropTypes.string,
    checked: PropTypes.bool,
    onChange: PropTypes.func,
}

Checkbox.defaultProps = {
    className: undefined,
    label: undefined,
    checked: false,
    onChange: undefined,
}
export default Checkbox;