import React from 'react';
import { shallow } from 'enzyme';
import App from './App';

describe('App comp', () => {
    const component = shallow(<App />);
    
    it('should render correctly on initial load', () => {
        expect(component).toMatchSnapshot();
    });
});
