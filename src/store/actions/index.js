export const SET_TODO_INPUT = 'SET_TODO_INPUT';
export const ADD_TODO = 'ADD_TODO';
export const SET_TODO_STATUS = 'SET_TODO_STATUS';
export const SET_STATUS_FILTER = 'SET_STATUS_FILTER';

export const setToDoInput = (val) => ({ type: SET_TODO_INPUT, payload: val });
export const addToDo = (toDo) => ({ type: ADD_TODO, payload: toDo });
export const setToDoStatus = (index, status) => ({ type: SET_TODO_STATUS, payload: { status, index } });
export const setStatusFilter = (status) => ({ type: SET_STATUS_FILTER, payload: status });
