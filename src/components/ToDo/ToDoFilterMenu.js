import React from 'react';
import PropTypes from 'prop-types';
import Row from '../Form/Row';
import Col from '../Form/Col';
import Radio from '../Form/Radio';
import status from '../../referenceData/status';

const ToDoFilterMenu = ({ statusFilter, setStatusFilter }) => (
    <Row className="text-left todo-list-item-row" >
        {[
            { label: 'All', status: status.ALL, size: 'sm-3' },
            { label: 'Complete', status: status.COMPLETE, size: 'sm-5' },
            { label: 'Active', status: status.ACTIVE, size: 'sm-4' },
        ].map(({ label, status, size }) => <Col size={size} key={`filter-menu-${status}`}>
            <Radio name="statusFilter" label={label}
                checked={statusFilter === status} onClick={() => setStatusFilter(status)} />
        </Col>)}
    </Row >
);

ToDoFilterMenu.propTypes = {
    statusFilter: PropTypes.string.isRequired,
    setStatusFilter: PropTypes.func.isRequired,
}

export default ToDoFilterMenu;

