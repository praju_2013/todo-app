import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import initialState from '../../../store/initialState';
import status from '../../../referenceData/status';
import ToDoFilterMenu from '../ToDoFilterMenu';

describe('ToDoFilterMenu', () => {
    const setStatusFilterStub = sinon.stub();
    it('should render correctly and trigger setToDo', () => {
        const component = shallow(<ToDoFilterMenu statusFilter={status.COMPLETE}
            setStatusFilter={setStatusFilterStub} />);

        expect(component).toMatchSnapshot();
        component.find('Radio').at(1).simulate('click', true);
        expect(setStatusFilterStub.calledOnce).toBe(true);
    });
});