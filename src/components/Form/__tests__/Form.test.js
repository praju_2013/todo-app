import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import Form from '../Form';

describe('Form', () => {
    const onSubmitStub = sinon.stub();
    const component = shallow(<Form onSubmit={onSubmitStub}>content</Form>);

    it('should render correctly', () => {
        expect(component).toMatchSnapshot();
        component.find('form').simulate('submit', { preventDefault: () => false });
        expect(onSubmitStub.calledOnce).toBe(true);
    });
});
